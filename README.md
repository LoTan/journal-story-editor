# The Journal : Story Editor

## Initial setup

**Mac OSX**:

* Install Node.js and npm [with homebrew](http://thechangelog.com/install-node-js-with-homebrew-on-os-x/)
* Install yeoman (which will include [bower](http://bower.io/) and [grunt](http://gruntjs.com/))

    >npm install -g yo

**Windows**:

* Install [NodeJS](http://nodejs.org/download/) (which should include NPM)
* Install yeoman

    >npm install -g yo

**Either platform**

You will ocassionally need to run this command during development, if the package.json file changes.  We will also need to run it now if this is your initial checkout:

>npm update

## Development

*All commands should be run from the root directory*

**Firing up the app**

Run the following command to fire up the app server:

>bower install && grunt debug:server

Open Chrome browser and head over to [http://localhost:9000](http://localhost:9000)

If all goes well, you should now see the app.  If you check your terminal, the 'watch' task should be running.  This means LiveReload will be active.  Any changes you make to the app will cause the open web page to be refreshed.  To open the debugging tool, just hit F12 in your browser.

**Adding dependencies**

Dependencies should be added by using bower and grunt.  Run the following commands to accomplish this:

>bower install --save <dependency_name>

>grunt bowerInstall

The bower command installs the dep into the local repo and adds it to the bower dependencies list.  The grunt command auto-injects it into the main HTML page.

*Read more about bower [on their site](http://bower.io/) if you have a problem locating dependency names*