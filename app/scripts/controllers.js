/*global angular:false */
/*global jQuery:false */
/*global LuaGenerator:false */
/*global toastr:false */
/*global bootbox:false */

'use strict';

var journalStoryApp = angular.module('journalStoryApp', ['ngRoute']);

journalStoryApp.directive('shortcut', function() {
    return {
        restrict: 'E',
        replace: true,
        scope: true,
        link:
            function postLink(scope){
                jQuery(document).on('keydown', function(e){
                    scope.$apply(scope.keyPressed(e));
                });
            }
    };
});

journalStoryApp.directive('selectpicker',  [
    '$timeout',
    function($timeout) {
        return {
            restrict: 'EA',
            link:
                function postLink(scope, iElement) {
                    var useBootstrapSelectPickers = function() {
                        jQuery(iElement).selectpicker();
                    };
                    $timeout(useBootstrapSelectPickers, 0);
                }
        };
    }
]);

// Set up the routes
journalStoryApp.config(['$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider){
    // Toastr config
    toastr.options = {
        'closeButton': false,
        'debug': false,
        //'positionClass': 'toast-bottom-full-width',
        'onclick': null,
        'showDuration': '300',
        'hideDuration': '1000',
        'timeOut': '5000',
        'extendedTimeOut': '1000',
        'showEasing': 'swing',
        'hideEasing': 'linear',
        'showMethod': 'fadeIn',
        'hideMethod': 'fadeOut'
    };

    $httpProvider.defaults.useXDomain = false;
    $routeProvider
        .when('/journal', {
            templateUrl: '/editor.html',
            controller: 'StoryEditorController',
            resolve: {
                loadedStory: function(){
                    return undefined;
                }
            }
        })
        .when('/journal/:storyId', {
            templateUrl: '/editor.html',
            controller: 'StoryEditorController',
            resolve: {
                loadedStory: function(storyService, $route){
                    return storyService.loadStory($route.current.params.storyId);
                }
            }
        })
        .when('/journal/:storyId/play', {
            templateUrl: '/playback.html',
            controller: 'PlaybackController'
        })
        .otherwise({
            redirectTo: '/journal'
        });
    $locationProvider.html5Mode(true);
}]);

journalStoryApp.controller('StoryEditorController', function ($scope, $routeParams, $timeout, $location, storyService, loadedStory) {
    var pageId = 1;
    var storyTitlePlaceholder = '<Story Title>';
    var pagesById = {};
    var defaultLowRoll = 5;
    var defaultHighRoll = 20;

    $scope.pageName = '';
    $scope.pages = [];
    $scope.selectedPage = undefined;
    $scope.editSection = '';
    $scope.loadedStories = [];
    $scope.storyTitle = loadedStory ? loadedStory.pages[0].title : undefined;
    $scope.choiceOptions = [
        {'name': 'charisma', 'code': 'cha', 'category': 'Trait'},
        {'name': 'dexterity', 'code': 'dex', 'category': 'Trait'},
        {'name': 'strength', 'code': 'str', 'category': 'Trait'},
        {'name': 'intelligence', 'code': 'int', 'category': 'Trait'},
        {'name': 'earth', 'code': 'earth', 'category': 'Elemental'},
        {'name': 'water', 'code': 'water', 'category': 'Elemental'},
        {'name': 'air', 'code': 'air', 'category': 'Elemental'},
        {'name': 'fire', 'code': 'fire', 'category':  'Elemental'},
        {'name': 'bow', 'code': 'bow', 'category': 'Weapon'},
        {'name': 'sword', 'code': 'sword', 'category': 'Weapon'},
        {'name': 'fight', 'code': 'fight', 'category': 'Weapon'},
        {'name': 'shield', 'code': 'shield', 'category': 'Weapon'}//,
        //{'name': 'direct-link', 'code': 'direct', 'category': 'Misc'}
    ];

    // If the story title changes, update the first page
    $scope.$watch('storyTitle', function(newTitle){
        if (!newTitle) {
            $scope.pages[0].title = '<Story Title>';
            return;
        }
        $scope.pages[0].title = newTitle;
    });

    $scope.$watch('pages[0].title', function(newTitle) {
        if (newTitle !== storyTitlePlaceholder) {
            $scope.storyTitle = newTitle;
        }
    });

    var registerPage = function(page) {
        $scope.pages.push(page);
        pagesById[page.id] = page;
        return page;
    };

    var registerPages = function(pages) {
        angular.forEach(pages, function(page){
            registerPage(page);
            if (page.id >= pageId) {
                pageId = page.id + 1;
            }
        });
    };

    var addPage = function(title) {
        var page = {
            'id': pageId++,
            'title': title,
            'hero': {
                'choices': []
            },
            'fail': {
                'choices': []
            }
        };
        return registerPage(page);
    };

    $scope.addPage = function() {
        if (!$scope.pageName) {
            return;
        }
        addPage($scope.pageName);
        $scope.pageName = '';
    };

    $scope.onSelect = function(page) {
        $scope.selectedPage = page;
        $scope.editSection = page.hero;
    };

    $scope.breadcrumbClass = function(page) {
        return page === $scope.selectedPage ? 'btn-primary' : 'btn-info';
    };

    $scope.isRemovable = function() {
        return $scope.pages.indexOf($scope.selectedPage) > 0;
    };

    $scope.removePage = function() {
        var idx = $scope.pages.indexOf($scope.selectedPage);
        $scope.pages.splice(idx, 1);

        if (idx > 0) {
            $scope.selectedPage = $scope.pages[idx - 1];
            $scope.editSection = $scope.selectedPage.hero;
        } else {
            $scope.selectedPage = undefined;
        }
    };

    $scope.editPageTitle = function() {
        $scope.editable = 'name';
        jQuery('#pageTitle').focus();
        setTimeout(
            function() {
                jQuery('#pageTitle').focus().select();
            }, 100);
    };

    $scope.editHero = function() {
        if ($scope.editSection !== $scope.selectedPage.hero) {
            $scope.editSection = $scope.selectedPage.hero;
        }
    };

    $scope.editFail = function() {
        if ($scope.editSection !== $scope.selectedPage.fail) {
            $scope.editSection = $scope.selectedPage.fail;
        }
    };

    $scope.editAdvanced = function() {
        if ($scope.editSection !== $scope.selectedPage.advanced) {
            $scope.editSection = $scope.selectedPage.advanced;
        }
    };

    $scope.createChoice = function() {
        $scope.editSection.choices.push({
            'rollLow': defaultLowRoll,
            'rollHigh': defaultHighRoll,
            'type': '',
            'label': ''
        });
    };

    $scope.deleteChoice = function(choice) {
        var idx = $scope.editSection.choices.indexOf(choice);
        if (idx >= 0) {
            $scope.editSection.choices.splice(idx, 1);
        }
    };
    
    $scope.keyPressed = function(e) {
        $scope.keyCode = document.activeElement.tagName;
        if (document.activeElement.tagName === 'BODY' ||
                document.activeElement.tagName === 'BUTTON') {
            var pageNum = e.which - 49;
            if (pageNum >= 0 && pageNum < $scope.pages.length) {
                $scope.onSelect($scope.pages[pageNum]);
            }

            var currentIdx = $scope.pages.indexOf($scope.selectedPage);
            if (e.which === 37) {
                if (currentIdx > 0) {
                    $scope.onSelect($scope.pages[currentIdx - 1]);
                }
            } else if (e.which === 39) {
                if (currentIdx < $scope.pages.length - 1) {
                    $scope.onSelect($scope.pages[currentIdx + 1]);
                }
            }
        }
    };

    $scope.hasAdvancedModeOption = function() {
        return $scope.selectedPage.isRoot;
    };

    $scope.editSectionTabClass = function(idx) {
        if (idx === 0 && $scope.editSection === $scope.selectedPage.hero) {
            return 'active';
        }

        if (idx === 1 && $scope.editSection === $scope.selectedPage.advanced) {
            return 'active';
        }

        if (idx === 3 && $scope.editSection === $scope.selectedPage.fail) {
            return 'active';
        }

        return '';
    };

    $scope.onSuccessPageChanged = function(choice) {
        if (!choice.fail) {
            choice.fail = choice.success;
        }
    };

    $scope.prefabButtonText = function() {
        return $scope.optionPrefabs && $scope.optionPrefabs[$scope.selectedPage.id] ? 'Update Prefab' : 'Create Prefab';
    };

    $scope.hasOptionPrefabs = function() {
        return $scope.optionPrefabs && $scope.optionPrefabs.length > 0;
    };

    $scope.createOptionPrefab = function() {
        bootbox.prompt('Enter prefab name', function(result) {
            if (result === null) {
                console.log('Prompt dismissed... not adding a prefab');
            } else if (result) {
                $scope.$apply(function(){
                    if (!$scope.optionPrefabs) {
                        $scope.optionPrefabs = [];
                    }
                    $scope.optionPrefabs.push({
                        name: result,
                        choices: $scope.editSection.choices
                    });
                    toastr.success('Prefab created');
                    console.log('Prefab ' + result + ' created, looks like this: ' + JSON.stringify($scope.editSection.choices));
                });
                
            }
        });
    };

    $scope.applyPrefab = function(prefab) {
        angular.forEach(prefab.choices, function(choice){
            if ($scope.editSection.choices.indexOf(choice) === -1) {
                $scope.editSection.choices.push(choice);
            }
        });
    };

    $scope.removeOptionPrefab = function(prefab) {
        var idx = $scope.optionPrefabs.indexOf(prefab);
        $scope.optionPrefabs.splice(idx, 1);
        toastr.success('Prefab deleted');
    };
    /******************************
     * Saving and loading stories
     *****************************/
     // The friendService returns a promise.
    function refreshStoryList() {
        storyService.storyList()
            .then(function (stories) {
                $scope.loadedStories = stories;
            }
        );
    }

    $scope.saveMyStory = function() {
        var storyToSave;
        if ($scope.activeStory) {
            console.log('updating existing story');
            storyToSave = $scope.activeStory;
            storyToSave.story = $scope.pages[0].title;
            storyToSave.optionPrefabs = $scope.optionPrefabs;
            storyToSave.pages = $scope.pages;
        } else {
            console.log('creating a new story!');
            storyToSave = {
                'story': $scope.pages[0].title,
                'pages': $scope.pages,
                'optionPrefabs': $scope.optionPrefabs
            };
        }
        storyService.saveStory(storyToSave)
            .then(function(story){
                if (story && story._id.$oid) {
                    refreshStoryList();
                    $location.path('/journal/' + story._id.$oid);
                    toastr.success('Story has now been saved! No worries.', 'Booyah!');
                }
            });
    };

    $scope.playStory = function() {
        if (!loadedStory) {
            toastr.info('Please save your story first.');
        } else {
            $location.path('/journal/' + loadedStory._id.$oid + '/play');
        }
    };

    $scope.deleteStory = function() {
        if (loadedStory) {
            bootbox.confirm({
                title: 'Delete \'' + loadedStory.story + '\'',
                message: 'Are you sure?  This action cannot be undone.',
                closeButton: 'false',
                buttons: {
                    cancel: {
                        label: 'Cancel',
                        className: 'btn-default',
                        callback: function(){
                            console.log('Canceling.');
                        }
                    },
                    confirm: {
                        label: 'OK',
                        className: 'btn-danger',
                        callback: function(){
                            console.log('deleting story.');
                        }
                    }
                },
                callback: function(result) {
                    if (result) {
                        console.log('Deleting this story: ' + loadedStory._id.$oid);
                        storyService.deleteStory(loadedStory._id.$oid)
                        .then(function(){
                            toastr.success('The story has been deleted.', 'You Asked For It');
                            refreshStoryList();
                            $location.path('/journal');
                        });
                    }
                    console.log('rawr:' + result);
                }
            });
        } else {
            toastr.warning('Stories must be saved before they can be deleted.');
        }
    };
    /** END STORY SERVICES */

    $scope.generateLua = function() {
        LuaGenerator.generateLua($scope.pages, function(codes){
            bootbox.dialog({
                title: 'The Codes',
                message: '<div class="row">  ' +
                    '<div class="col-md-12 col-lg-12"> ' +
                    '<div class="form-group page-content-section">' +
                    '<textarea id="storyInput" rows="8" ng-model="editSection.content" class="form-control">' + codes + '</textarea>' +
                    '</div>' +
                    '</div></div>',
                buttons: {
                    success: {
                        label: 'Close',
                        className: 'btn-danger'
                    }
                }
            }).find('div.modal-dialog').addClass('lua-code-box');
        });
    };

    function createRootPage() {
        var page = addPage(storyTitlePlaceholder);
        page.isRoot = true;
        page.advanced = {
            choices: []
        };

        // Select the root page
        $scope.onSelect(page);
    }

    function enableAutoComplete() {
        function wireAutoCompleteHandler(){
            jQuery('#storyInput').textcomplete([
                {
                    words: ['player_name', 'town', 'npc_name', 'story_stuff', 'etc'],
                    match: /<(\w*)$/,
                    search: function (term, callback) {
                        callback(jQuery.map(this.words, function (word) {
                            return word.indexOf(term) === 0 ? word : null;
                        }));
                    },
                    index: 1,
                    replace: function (word) {
                        return '<' + word + '> ';
                    }
                }
            ]);
        }
        $scope.$watch('editSection', wireAutoCompleteHandler);
        $timeout(wireAutoCompleteHandler, 200);
    }

    function init() {
        enableAutoComplete();
        if (!loadedStory) {
            createRootPage();
        } else {
            console.log('Story loaded: ' + JSON.stringify(loadedStory));
            $scope.activeStory = loadedStory;
            $scope.optionPrefabs = loadedStory.optionPrefabs;
            registerPages(loadedStory.pages);
            $scope.onSelect($scope.pages[0]);
        }
        refreshStoryList(); // load saved stories!
    }

    init();
});

journalStoryApp.controller('PlaybackController', function ($scope) {
    $scope.message = 'Playback coming soon!';
});

journalStoryApp.service('storyService', function($http, $q){
    var apiKey = 'f_BQYD0v1Pthz-UilvjyMmN2AlE9GyST';

    // Is there a sprintf/String.format to make this prettier?
    //var storyDocumentsUri = host + '/deployments/' + slug + '/' + deploymentId + '/mongodb/' + databaseName + '/collections/' + collectionName + '/documents';
    var storyDocumentsUri = 'https://api.mongolab.com/api/1/databases/journal_stories/collections/stories';

    var apiKeyParam = '?apiKey=' + apiKey;

    function getDocumentUri(id) {
        return storyDocumentsUri + '/' + id + apiKeyParam;
    }

    function storyList() {
        var request = $http.get(storyDocumentsUri + apiKeyParam + '&f={"story":1}');
        return (request.then(handleSuccess, handleError));
    }

    function saveStory(story) {
        var request;
        if (story._id) {
            request = $http.put(getDocumentUri(story._id.$oid),{
                'story': story.story,
                'pages': story.pages,
                'optionPrefabs': story.optionPrefabs
            });
            return (request.then(handleSuccess, handleError));
        } else {
            request = $http.post(storyDocumentsUri + apiKeyParam, story);
        }
        return (request.then(handleSuccess, handleError));
    }

    function loadStory(id) {
        console.log('Loading story with id ' + id);
        var request = $http.get(getDocumentUri(id));
        return (request.then(handleSuccess, handleError));
    }

    function deleteStory(id) {
        console.log('Deleting story with id ' + id);
        var request = $http.delete(getDocumentUri(id));
        return (request.then(handleSuccess, handleError));
    }

    // I transform the error response, unwrapping the application dta from
    // the API response payload.
    function handleError( response ) {
        // The API response from the server should be returned in a
        // nomralized format. However, if the request was not handled by the
        // server (or what not handles properly - ex. server error), then we
        // may have to normalize it on our end, as best we can.
        if (
            ! angular.isObject( response.data ) ||
            ! response.data.message
            ) {
            return( $q.reject( 'An unknown error occurred.' ) );
        }
        // Otherwise, use expected error message.
        return( $q.reject( response.data.message ) );

    }

    // I transform the successful response, unwrapping the application data
    // from the API response payload.
    function handleSuccess( response ) {
        return( response.data );
    }

    return {
        storyList: storyList,
        loadStory: loadStory,
        saveStory: saveStory,
        deleteStory: deleteStory
    };
});