/*global nunjucks:false */

'use strict';

(function(){
	var storyTemplate = 'story.html',
		env = new nunjucks.Environment(new nunjucks.WebLoader('/lua_generator')),
	    pageIdMap = {};

	function toVariableName(str){
		return str.toLowerCase().replace(/ /g, '_');
	}

	function createFragmentVar(pageName) {
		return toVariableName(pageName) + '_pages';
	}

	function createStoryVar(storyTitle) {
		return toVariableName(storyTitle) + '_story';
	}

	function generateLuaScript(story, callback) {
		return env.render(storyTemplate, story, function(err, res){
			if (err) {
				console.log('error rendering template: ' + err);
			} else {
				//console.log('\nGeneration complete, Lua =\n\n' + res);
				callback(res);
			}
		});
	}

	function createFragmentAndParticles(arr, page, mode) {
		var fragmentData, fragmentTitle, fragmentVar, storyVar, concatTitle, lookupKey;
		switch (mode) {
			case 0:
				// hero
				fragmentData = page.hero;
				fragmentTitle = page.title + ' (Hero)';
				concatTitle = page.title + ' hero';
				lookupKey = page.id + '.success';
				break;
			case 1:
				// adv
				fragmentData = page.advanced;
				fragmentTitle = page.title + ' (Advanced)';
				concatTitle = page.title + ' advanced';
				break;
			case 2:
				// fail
				fragmentData = page.fail;
				fragmentTitle = page.title + ' (Fail)';
				concatTitle = page.title + ' fail';
				lookupKey = page.id + '.fail';
				break;
		}

		function trimReplaceNewlines(strArray) {
			for (var i = 0; i < strArray.length; i++) {
				strArray[i] = strArray[i].replace(/^\s+|\s+$/g, '').replace(/\r\n|\r|\n/g,'\\n');
			}
			return strArray;
		}

		if (fragmentData && fragmentData.content) {
			//console.log(fragmentTitle + ' has content.');
			fragmentVar = createFragmentVar(concatTitle);
			storyVar = createStoryVar(concatTitle);

			if (lookupKey) {
				pageIdMap[lookupKey] = fragmentVar;
			}

			arr.push({
				title: fragmentTitle,
				storyLabel: fragmentData.label || page.title,
				storyVarName: storyVar,
				varName: fragmentVar,
				particles: trimReplaceNewlines(fragmentData.content.split('---')),
				choices: fragmentData.choices
			});
		}
	}

	/**
	 * Change the simple array into something more meaningful to the generator.
	 */
	function storyAsContext(storyPages) {
		// reset the page id map
		pageIdMap = {};

		var storyContext = {
			story: storyPages[0].title,
			varName: createStoryVar(storyPages[0].title),
			fragments: []
		};

		for (var i = 0; i < storyPages.length; i++) {
			// Each page from the editor's point of view could have up to 3 modes.  We need
			// to generate the pages and choices and fragments for each page and its variations.
			for (var j = 0; j < 3; j++) {
				createFragmentAndParticles(storyContext.fragments, storyPages[i], j);
			}
		}

		storyContext.pageIdMap = pageIdMap;

		return storyContext;
	}

	window.LuaGenerator = ({
		'generateLua': function(storyPages, callback) {
			var storyContext = storyAsContext(storyPages);
			return generateLuaScript(storyContext, callback);
		}
	});
})();